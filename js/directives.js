var directives = angular.module("directives", []);

directives.directive("fileinput", function($http, $compile){
    return{
        restrict: "E",
        link: function(scope, element, attributes){
        },
        templateUrl: "partials/fileinput.html"
    }
});

directives.directive("employers", function($http, $compile){
    return{
        restrict: "E",
        link: function(scope, element, attributes){
        },
        templateUrl: "partials/employers.html"
    }
});