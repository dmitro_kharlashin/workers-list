var factories = angular.module('factories', []);

    factories.factory('employers', function ($http) {
        return {
            model: {},
            file_changed: function(filesPath, $scope){
                var _self = this;
                if(!filesPath.files.length){
                    $scope.showTable();
                    $scope.hideLoader();
                }
                _self.prepareFilesContent(filesPath, $scope);
            },
            list: function ($scope) {
                var _self = this;
                var sql = "SELECT emp.*, ea.*, man.name as manager FROM Employers emp " +
                    "LEFT JOIN EmployersAttendance ea ON ea.id = emp.id " +
                    "LEFT JOIN Managers man ON man.id = emp.man_id " +
                    "GROUP BY emp.id, ea.date;";
                Database.db.transaction(function(tx) {
                        tx.executeSql(sql, [], function (t, rows) {
                            _self.prepareEmployerObject($scope, rows.rows);
                        }, function (tx, error) {
                            console.log(error);
                        });
                    tx.executeSql("SELECT MAX(date) as period_out, MIN(date) as period_in FROM EmployersAttendance", [], function (t, rows) {
                        var period_in = rows.rows.item(0).period_in.split('.');
                        var period_out = rows.rows.item(0).period_out.split('.');
                        $scope.period_in = period_in.reverse().join('-');
                        $scope.period_out = period_out.reverse().join('-');
                        $scope.showTable();
                        $scope.hideLoader();
                    }, function (tx, error) {
                        console.log(error);
                    });
                });
            },
            prepareFilesContent: function(filePath, $scope){
                var _self = this;
                var output = "";
                if(filePath.files) {
                    angular.forEach(filePath.files, function(value, key){
                        FR.checkFileAPI();
                        FR.reader.onload = function (e) {
                            output = e.target.result;
                            _self.pushContentToDB($scope, key, output, filePath.files);
                        };
                        FR.reader.readAsText(filePath.files[key]);
                    });
                }
                else {
                    return false;
                }
                return true;

            },
            pushContentToDB: function($scope, key, request, files){
                var _self = this;
                var peopleDay = [];
                var dataRows = request.split('\n');
//                var dataRows = Utf8.decode(request.responseText).split('\n');
//                var dataRows = unescape( encodeURIComponent( request.responseText ) ).split('\n');
//                var dataRows = decodeURIComponent( escape( request.responseText ) ).split('\n');
//                console.log(unescape( encodeURIComponent( request.responseText ) ));
//                console.log(decodeURIComponent( request.responseText ));
//                console.log(decodeURIComponent( Utf8.encode(request.responseText) ));
                dataRows.splice(0,1);
                for(var i in dataRows){
                    if(dataRows[i] == '') {
                        continue;
                    }
//                  var re = /(\d+)+\s*(.*)\s(\d{2}[.]\d{2}[.]\d{4})+\s(?:(\d{2}:\d{2})\s*(\d{2}:\d{2})|.*)/;
                    var re = /(\d+)+\s*(.*)\s(\d{2}[.]\d{2}[.]\d{4})+\s(?:(\d{2}:\d{2})\s*(\d{2}:\d{2})|(\d{2}:\d{2})| \s+(\d{2}:\d{2})|.*)/;
                    var matchRows = dataRows[i].match(re);
                    if (!matchRows){
                        $scope.hideLoader();
                        $scope.showAlert('Ошибка загрузки файла');
                        return false;
                    }
                    var postData ={};
                    postData.id = matchRows[1];
                    postData.name = matchRows[2];
                    postData.date = matchRows[3].split('.').reverse().join('-');
                    if (!matchRows[4] || !matchRows[5])
                        postData.absent = 1;
                    else {
                        postData.time_in = matchRows[4];
                        postData.time_out = matchRows[5];
                    }
                    if(matchRows[6])
                        postData.time_in = matchRows[6];
                    if(matchRows[7])
                        postData.time_out = matchRows[7];
                    var json_data = angular.toJson(postData);
                    if(employers_str.search(json_data) == -1){
                        employers_str += json_data;
                        peopleDay.push(postData);
                    }
                }
                localStorage['employers'] = employers_str;
                Database.createEmployersTable(peopleDay, $scope);
                if (files.length - 1 == key) _self.list($scope);
            },
            prepareEmployerObject: function($scope, employers){
                var _self = this;

                for (var i = 0; i < employers.length; i++){
                    var value = employers.item(i);
                    var date = value.date/*.split('.').reverse().join('-')*/;
                    if($scope.employers[value.id] != undefined && $scope.employers[value.id].date != undefined && $scope.employers[value.id].date[date] != undefined) continue;
                    var timeInHours = (value.time_in != null)
                        ? parseInt(value.time_in.split(':')[0]) + (parseInt(value.time_in.split(':')[1])/60)
                        : 0;
                    var timeOutHours = (value.time_out != null)
                        ? parseInt(value.time_out.split(':')[0]) + (parseInt(value.time_out.split(':')[1])/60)
                        : 0;
                    var shiftLength = (timeInHours != 0 && timeOutHours != 0) ? timeOutHours - timeInHours : 0;
                    if($scope.employers[value.id] == undefined){
                        $scope.employers[value.id] = {
                            id: value.id,
                            name: value.name,
                            date: [],
                            absentDays: value.absent,
                            timeWorked: shiftLength,
                            manager: {
                                id: value.man_id,
                                name: value.manager
                            }
                        };
                    } else {
                        $scope.employers[value.id].absentDays += value.absent;
                        $scope.employers[value.id].timeWorked += shiftLength;
                    }
                    $scope.employers[value.id].date[date] = {
                        time_in: value.time_in,
                        time_out: value.time_out,
                        shiftLength: shiftLength,
                        parsedShiftLength: _self.parseTimeToString(shiftLength),
                        absent: value.absent,
                        timeInHours: timeInHours,
                        timeOutHours: timeOutHours
                    };
                }
                _self.constructTimingParams($scope.employers);
            },
            constructTimingParams: function(employers){
                var _self = this;
                angular.forEach(employers, function(value, key){
                    this[value.id].shiftAVG = (this[value.id].timeWorked)/(Object.keys(this[value.id].date).length - this[value.id].absentDays);
                    this[value.id].parsedShiftAVG = _self.parseTimeToString(this[value.id].shiftAVG);
                    this[value.id].parsedTimeWorked = _self.parseTimeToString(this[value.id].timeWorked);
                    var sumTimeInHours = 0;
                    var sumTimeOutHours = 0;
                    var countShiftDays = 0;
                    for (var date in this[value.id].date){
                        if ((this[value.id].date[date].timeInHours != 0 && this[value.id].date[date].timeOutHours != 0)){
                            sumTimeInHours += this[value.id].date[date].timeInHours;
                            sumTimeOutHours += this[value.id].date[date].timeOutHours;
                            countShiftDays ++;
                        }
                    }
                    this[value.id].timeInAVG = sumTimeInHours/countShiftDays;
                    this[value.id].timeOutAVG = sumTimeOutHours/countShiftDays;
                    this[value.id].parsedTimeInAVG = _self.parseTimeToString(this[value.id].timeInAVG);
                    this[value.id].parsedTimeOutAVG = _self.parseTimeToString(this[value.id].timeOutAVG);
                }, employers);
            },
            parseTimeToString: function(time){
                if(!time) return '00:00';
                var hours = Math.floor(time);
                var minutes = Math.floor((time - hours)*60);
                return ((hours < 10) ? '0'+ hours : hours)+':'+ ((minutes < 10) ? '0'+ minutes : minutes);
            },
            constructFilteredByPeriodData: function(employer, dateIn, dateOut, parsedTimeInMin, parsedTimeInMax){
                var _self = this;
                employer.absentDays = 0;
                employer.timeWorked = 0;
                var sumTimeInHours = 0;
                var sumTimeOutHours = 0;
                var countShiftDays = 0;
                for(var date in employer.date){
                    var d = new Date(date).setHours(0, 0);
                    if (d >= dateIn && d <= dateOut){
                        employer.absentDays += employer.date[date].absent;
                        employer.timeWorked += employer.date[date].shiftLength;
                        if (employer.date[date].timeInHours != 0 && employer.date[date].timeOutHours != 0) {
                            sumTimeInHours += employer.date[date].timeInHours;
                            sumTimeOutHours += employer.date[date].timeOutHours;
                            countShiftDays ++;
                        }
                    }
                    else if (d < dateIn || d > dateOut){
                        employer.date.splice(employer.date.indexOf(employer.date[date]), 1);
                    }
                }
                employer.parsedTimeWorked = _self.parseTimeToString(employer.timeWorked);
                employer.shiftAVG = (employer.timeWorked)/countShiftDays;
                employer.parsedShiftAVG = _self.parseTimeToString(employer.shiftAVG);
                employer.timeInAVG = sumTimeInHours/countShiftDays;
                employer.timeOutAVG = sumTimeOutHours/countShiftDays;
                employer.parsedTimeInAVG = _self.parseTimeToString(employer.timeInAVG);
                employer.parsedTimeOutAVG = _self.parseTimeToString(employer.timeOutAVG);
                var filteredByMinTimeIn = (!parsedTimeInMin) ? true : (employer.timeInAVG >= parsedTimeInMin);
                var filteredByMaxTimeIn = (!parsedTimeInMax) ? true : (employer.timeInAVG <= parsedTimeInMax);
                var filteredByTimeIn = filteredByMaxTimeIn && filteredByMinTimeIn;

                return (!filteredByTimeIn) ? false : employer;
            },
            filter: function($scope, value, employer_name, manager_name, date_in, date_out, parsedTimeInMin, parsedTimeInMax){
                var _self = this;
                var filteredByMinTimeIn = (!parsedTimeInMin) ? true : (value.timeInAVG >= parsedTimeInMin);
                var filteredByMaxTimeIn = (!parsedTimeInMax) ? true : (value.timeInAVG <= parsedTimeInMax);
                var filteredByTimeIn = filteredByMaxTimeIn && filteredByMinTimeIn;
                if (!date_in && !date_out && !employer_name && !manager_name) {
                    if(filteredByTimeIn) return value;
                } else{
                    if(!employer_name && !manager_name){
                        var dateIn = (!date_in)
                            ? new Date($scope.period_in).setHours(0, 0) : new Date(date_in);
                        var dateOut = (!date_out)
                            ? new Date($scope.period_out).setHours(0, 0) : new Date(date_out);
                        return _self.constructFilteredByPeriodData(value, dateIn, dateOut, parsedTimeInMin, parsedTimeInMax);
                    }else {
                        var employers_match = true;
                        var managers_match = true;
                        if(employer_name != undefined){
                            var re = new RegExp('.*('+employer_name+')+.*', 'gi');
                            var name = value.name;
                            employers_match = name.match(re);
                        }
                        if(manager_name != undefined){
                            var re = new RegExp('.*('+manager_name+')+.*', 'gi');
                            var name = value.manager.name;
                            if(name != null)
                                managers_match = name.match(re);
                            else
                                managers_match = false;
                        }
                        if(employers_match && managers_match){
                            if (!date_in && !date_out) {
                                if(filteredByTimeIn) return value;
                            }
                            else{
                                var dateIn = (!date_in)
                                    ? new Date($scope.period_in).setHours(0, 0) : new Date(date_in);
                                var dateOut = (!date_out)
                                    ? new Date($scope.period_out).setHours(0, 0) : new Date(date_out);
                                return _self.constructFilteredByPeriodData(value, dateIn, dateOut, parsedTimeInMin, parsedTimeInMax);
                            }
                        }
                    }
                }
                return false;
            }
        }
    });

    factories.factory('managers', function ($http) {
        return {
            list: function ($scope) {
                var _self = this;
                var sql = "SELECT * FROM Managers man";
                Database.db.transaction(function(tx) {
                    tx.executeSql(sql, [], function (t, rows) {
                        _self.prepareManagerObject($scope, rows.rows);
                    }, function (tx, error) {
                        console.log(error);
                    });
                });
            },
            prepareManagerObject: function($scope, managers){
                var _self = this;
                for (var i = 0; i < managers.length; i++){
                    var value = managers.item(i);
                    var json_data = angular.toJson(value);
                    if(managers_str.search(json_data) == -1){
                        managers_str += json_data;
                    }
                    $scope.managers[value.id] = {
                        id: value.id,
                        name: value.name
                    };
                }
                localStorage['managers'] = managers_str;
                $scope.$digest();
            },
            addContentToDB: function($scope, id, name){
                if(managers_str.search(name) == -1){
                    var manager = {
                        id: id,
                        name: name
                    };
                    var json_data = angular.toJson(manager);
                    managers_str += json_data;
                    Database.addManager(manager);
                    $scope.managers[id] = manager;
                    localStorage['managers'] = managers_str;
                }
            }
        }
    });