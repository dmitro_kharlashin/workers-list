var scope = [];

var controllers = angular.module("controllers", []);

controllers.controller("SiteController", ["$scope", "$modal", "employers", "managers", function($scope, $modal, employers, managers){

    $scope.employers = [];
    $scope.managers = [];
    $scope.period_in = '';
    $scope.period_out = '';
    $scope.isEmpty = function(){
        return $scope.employers.length == 0;
    };
    $scope.noManagers = function(){
        return !$scope.managers.length || localStorage['managers'] == '';
    };
    $scope.showManager = function(id){
        location.replace('#/manager/'+ id);
    };
    $scope.detalization = function(id){
        location.replace('#/employer/'+ id);
    };
    $scope.hideTable = function(){
        angular.element(document.querySelector('employers')).removeClass('ng-show').addClass('ng-hide');
    };
    $scope.showTable = function(){
        $scope.$digest();
        angular.element(document.querySelector('employers')).removeClass('ng-hide').addClass('ng-show');
    };
    $scope.showLoader = function(){
        $scope.modalInstance = $modal.open({
            animation: true,
            templateUrl: 'partials/progressbar.html',
            backdrop: false,
            keyboard: false
        });
    };
    $scope.hideLoader = function(){
        $scope.modalInstance.close();
    };
    $scope.showAlert = function(msg){
        alert(msg);
    };
    $scope.clearDb = function(){
        Database.clear();
        $scope.employers = [];
        localStorage.removeItem('employers');
        employers_str = '';
        angular.element(document.getElementById('inputFileToRead')).after(angular.element(document.getElementById('inputFileToRead')).clone()).remove();
    };
    $scope.loadManagers = function(){
        managers_str = localStorage.length && localStorage['managers'] != undefined ? localStorage['managers'] : '' ;
        if (managers_str && $scope.noManagers()) {
            managers.list($scope);
        }
    };
    $scope.loadEmployers = function(){
        Database.init();
        employers_str = localStorage.length && localStorage['employers'] != undefined ? localStorage['employers'] : '' ;
        if (employers_str && $scope.isEmpty()) {
            $scope.showLoader();
            employers.list($scope);
        }
        $scope.loadManagers();
    }();
}]);

controllers.controller("EmployersListController", ["$scope", "$modal", "employers", function($scope, $modal, employers){

    $scope.sortEmployers='id';
    $scope.file_changed = function(files){
        $scope.showLoader();
        $scope.hideTable();
        employers.file_changed(files, $scope);
    };
    $scope.search = function(employer_name, manager_name, date_in, date_out, time_in_min, time_in_max){
        var searchedData = [];
        var parsedTimeInMin = (!time_in_min) ? null : new Date(time_in_min).getHours() + (new Date(time_in_min).getMinutes()/60);
        var parsedTimeInMax = (!time_in_max) ? null : new Date(time_in_max).getHours() + (new Date(time_in_max).getMinutes()/60);
        angular.forEach($scope.employers, function(value){
            var filtredEmployer = employers.filter($scope, value, employer_name, manager_name, date_in, date_out, parsedTimeInMin, parsedTimeInMax);
            if (filtredEmployer)
                searchedData.push(filtredEmployer);
        });
        return searchedData;
    }
}]);

controllers.controller("EmployerInfoController", ['$scope', '$routeParams', function($scope, $routeParams){
    var link = location.hash;
    var id = (link.match(/[#][/]employer[/](\d+)/)) ? link.match(/[#][/]employer[/](\d+)/)[1] : $routeParams.id;
    $scope.employer = $scope.employers[id];
    if (!$scope.employer) location.replace('#/employers');
    $scope.employer_id = id;
    $scope.sortField='date';
    $scope.showByDate = function(employer_id, time_in_min, time_in_max){
        if(!employer_id) return [];
        var date = [];
        var parsedTimeInMin = (!time_in_min) ? NaN : new Date(time_in_min).getHours() + (new Date(time_in_min).getMinutes()/60);
        var parsedTimeInMax = (!time_in_max) ? NaN : new Date(time_in_max).getHours() + (new Date(time_in_max).getMinutes()/60);
        for(var i in $scope.employers[employer_id].date){
            var currentDate = $scope.employers[employer_id].date[i];
            var filter = (isNaN(parsedTimeInMin) && isNaN(parsedTimeInMax)) ? true : false;
            if (!isNaN(parsedTimeInMin) && !isNaN(parsedTimeInMax)){
                filter = currentDate.timeInHours != 0 && currentDate.timeInHours >= parsedTimeInMin && currentDate.timeInHours <= parsedTimeInMax;
            } else if (!isNaN(parsedTimeInMin) || !isNaN(parsedTimeInMax)) {
                filter = (!isNaN(parsedTimeInMin)) ? (currentDate.timeInHours != 0 && currentDate.timeInHours >= parsedTimeInMin) : (currentDate.timeInHours != 0 && currentDate.timeInHours <= parsedTimeInMax)    ;
            }
            if (filter == true) {
                var prepearedObject = currentDate;
                prepearedObject.date = i;
                date.push(prepearedObject);
            }
        }
        return date;
    }
}]);

controllers.controller("PageController", ['$scope', function($scope){
    $scope.page = (!localStorage && localStorage['tab'] == undefined) ? 'employers' : localStorage['tab'];
    $scope.setPage = function(tab){
        $scope.page = tab;
        localStorage['tab'] = tab;
    }
    $scope.setPage($scope.page);
    $scope.isSelected = function(tab){
        return $scope.page === tab;
    }
}]);

controllers.controller("ManagerController", ['$scope', 'managers', function($scope, managers){
    $scope.sortField='id';
    $scope.name = '';
    $scope.saveManager = function(){
        if (!$scope.name.length || managers_str.search($scope.name) != -1) return;
        var maxid = 0;
        $scope.managers.map(function(obj){
            if (obj.id > maxid) maxid = obj.id;
        });
        var newId = maxid + 1;
        var manager = $scope.name.trim();
        managers.addContentToDB($scope, newId, manager);
        $scope.name = '';
    };
    $scope.deleteManager = function(manager){
        Database.deleteManager(manager);
        angular.forEach($scope.employers, function(value, key){
            if(value.manager.id == manager.id){
                $scope.employers[key].manager = {
                    id: null,
                    name: null
                }
            }
        });
        var json_str = angular.toJson($scope.managers[manager.id]);
        managers_str = managers_str.replace(json_str, '');
        localStorage['managers'] = managers_str;
        $scope.managers[manager.id] = {
            id: null,
            name: null
        }
    };
    $scope.managersList = function(){
        var searchedData = [];
        angular.forEach($scope.managers, function(value){
            if (value.id != null && value.name != null)
                searchedData.push(value);
        });
        return searchedData;
    };
}]);

controllers.controller("ManagerInfoController", ['$scope', '$routeParams', function($scope, $routeParams){
    $scope.showAllEmployers = function(){
        var filteredSearch = [];
        angular.forEach($scope.unManagedEmployers, function(value){
            filteredSearch.push(value);
        });
        return filteredSearch;
    }
    $scope.showMyEmployers = function(){
        var filteredSearch = [];
        angular.forEach($scope.managedEmployers, function(value){
            filteredSearch.push(value);
        });
        return filteredSearch;
    }
    $scope.addEmployerToManager = function(manager, employer){
        Database.addEmployerToManager(manager, employer);
        var unManagedEmployers = [];
        angular.forEach($scope.unManagedEmployers, function(value, key){
            if(key != employer) unManagedEmployers[key] = value;
            else $scope.managedEmployers[employer] = value;
        });
        $scope.unManagedEmployers = unManagedEmployers;
        $scope.employers[employer].manager = {
            id: $scope.managers[manager].id,
            name: $scope.managers[manager].name
        }
    }
    $scope.deleteEmployerFromManager = function(manager, employer){
        Database.deleteEmployerFromManager(manager, employer);
        var managedEmployers = [];
        angular.forEach($scope.managedEmployers, function(value, key){
            if(key != employer) managedEmployers[key] = value;
            else $scope.unManagedEmployers[employer] = value;
        });
        $scope.managedEmployers = managedEmployers;
        $scope.employers[employer].manager = {
            id: null,
            name: null
        }
    }
    $scope.loadManagerDetails = function(){
        var link = location.hash;
        var id = (link.match(/[#][/]manager[/](\d+)/)) ? link.match(/[#][/]manager[/](\d+)/)[1] : $routeParams.id;
        $scope.manager = $scope.managers[id];
        if (!$scope.manager) location.replace('#/employers');
        $scope.manager_id = id;
        $scope.sortManagedEmployers='id';
        $scope.sortUnManagedEmployers='id';
        Database.searchForAllEmployers($scope, $scope.manager);
        Database.searchForManagersEmployers($scope, $scope.manager);
    }();
}]);
