var Database = {
    db: openDatabase('mydb', '1.0', 'Test DB', 2 * 1024 * 1024),
    init: function(){
        var self = this;
        self.db.transaction(function (tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS Employers (id INT(11) UNIQUE, name TEXT(255), man_id INT(11) DEFAULT NULL)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS EmployersAttendance (id INT(11), date VARCHAR, time_in REAL DEFAULT NULL, time_out REAL DEFAULT NULL, absent INT(1) DEFAULT '0')");
            tx.executeSql("CREATE TABLE IF NOT EXISTS Managers (id INT(11) UNIQUE, name TEXT(255))");
        });
    },
    createEmployersTable: function(data, $scope){
        var self = this;
        self.db.transaction(function (tx) {
            var keys= [];
            angular.forEach(data, function(value, key){
                if ((keys == [] || keys.indexOf(value.id) == -1) && ($scope.employers != undefined && $scope.employers[value.id] == undefined)){
                    tx.executeSql("INSERT INTO Employers (id, name) VALUES(?, ?)", [value.id, value.name], null, null);
                    keys.push(value.id);
                }
                self.fillEmployersAttendanceTable(tx, value);

            });
        });
    },
    fillEmployersAttendanceTable: function(tx, data){
        tx.executeSql("INSERT INTO EmployersAttendance (id, date, time_in, time_out, absent) VALUES(?, ?, ?, ?, ?)", [data.id, data.date, (data.time_in!=undefined)?data.time_in:null, (data.time_out!=undefined)?data.time_out:null, (data.absent!=undefined)?data.absent:0], null, null);
    },
    addManager: function(manager){
        var self = this;
        self.db.transaction(function (tx) {
            tx.executeSql("INSERT INTO Managers (id, name) VALUES(?, ?)", [manager.id, manager.name], null, null);
        });
    },
    deleteManager: function(manager){
        var self = this;
        self.db.transaction(function (tx) {
            tx.executeSql('DELETE FROM Managers WHERE id = ? AND name = ?', [manager.id, manager.name], null, null);
            tx.executeSql("UPDATE Employers SET man_id = ? WHERE man_id = ?", [null, manager.id], null, null);
        });
    },
    clear: function(){
        var self = this;
        self.db.transaction(function (tx) {
            tx.executeSql('DELETE FROM Employers');
            tx.executeSql('DELETE FROM EmployersAttendance');
        });
    },
    searchForAllEmployers: function($scope, manager){
        var sql = 'SELECT emp.id, emp.name FROM Employers emp' +
            ' LEFT JOIN Managers man ON man.id = emp.man_id' +
            ' WHERE emp.man_id IS NULL' +
            ' OR emp.man_id != '+ manager.id +
            ' AND emp.name != "'+ manager.name +'";';
        Database.db.transaction(function (tx) {
            tx.executeSql(sql, [], function (t, rows) {
                $scope.unManagedEmployers = [];
                for (var i = 0; i < rows.rows.length; i++){
                    var value = rows.rows.item(i);
                    $scope.unManagedEmployers[value.id] = value;
                }
                $scope.$digest();
            }, function (tx, error) {
                console.log(error);
            });
        });
    },
    searchForManagersEmployers: function($scope, manager){
        var sql = 'SELECT emp.id, emp.name FROM Employers emp' +
            ' LEFT JOIN Managers man ON man.id = emp.man_id' +
            ' WHERE emp.man_id == '+ manager.id +
            ' AND emp.name != "'+ manager.name +'";';
        Database.db.transaction(function (tx) {
            tx.executeSql(sql, [], function (t, rows) {
                $scope.managedEmployers = [];
                for (var i = 0; i < rows.rows.length; i++){
                    var value = rows.rows.item(i);
                    $scope.managedEmployers[value.id] = value;
                }
                $scope.$digest();
            }, function (tx, error) {
                console.log(error);
            });
        });
    },
    addEmployerToManager: function(manager, employer){
        Database.db.transaction(function (tx) {
            tx.executeSql("UPDATE Employers SET man_id = ? WHERE id = ?", [manager, employer], null, null);
        });
    },
    deleteEmployerFromManager: function(manager, employer){
        Database.db.transaction(function (tx) {
            tx.executeSql("UPDATE Employers SET man_id = ? WHERE id = ?", [null, employer], null, null);
        });
    }
};
