(function() {

    var app = angular.module("app", ['ui.bootstrap','ngRoute', 'controllers', 'factories', 'directives']);

    app.config(function($routeProvider) {
        $routeProvider.
            when('/', {
                redirectTo: '/employers'
            }).
            when('/employer/:id', {
                templateUrl: "partials/employer_details.html",
                controller: "EmployerInfoController"
            }).
            when('/employers', {
                templateUrl: "partials/main.html",
                controller: "EmployersListController"
            }).
            when('/managers', {
                templateUrl: "partials/managers.html",
                controller: "ManagerController"
            }).
            when('/manager/:id', {
                templateUrl: "partials/manager_details.html",
                controller: "ManagerInfoController"
            }).
            otherwise({
                redirectTo: '/employers'
            });
    });


})();