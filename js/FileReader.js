var FR = {
    /**
     * Check for the various File API support.
     */
    checkFileAPI: function() {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            //GLOBAL File Reader object for demo purpose only
            FR.reader = new FileReader();
            return true;
        } else {
            alert('The File APIs are not fully supported by your browser. Fallback required.');
            return false;
        }
    }

};
